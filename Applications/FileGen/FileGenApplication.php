<?php
namespace Applications\FileGen;

use \OCFram\Application;

class FileGenApplication extends Application
{
   public function __construct()
   {
      parent::__construct();

      $this->name = 'FileGen';
   }

   public function run()
   {
      $controller = $this->getController();
      $controller->execute();

      $this->httpResponse->setPage($controller->page());
      $this->httpResponse->send();
   }
}

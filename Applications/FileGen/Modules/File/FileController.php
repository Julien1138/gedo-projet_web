<?php
namespace Applications\FileGen\Modules\File;

use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \GedoFile\GedoFile;

class FileController extends BackController
{
   public function executeGenerate(HTTPRequest $request)
   {
      $response = '';
      
      if ($request->postExists('WateringZones') && $request->postExists('ArchiveId'))
      {
         $gedoFile = new GedoFile($request->postData('ArchiveId'));
         
         if ($gedoFile->createConfigFile(json_decode($request->postData('WateringZones'))))
         {
            if ($gedoFile->createArchive()) {
               $response = 'OK';
            }
            else
            {
               $response = 'Erreur';
            }
         }
         else
         {
            $response = 'Erreur';
         }
      }
      else
      {
         $response = 'Erreur';
      }
      
      $this->page->addVar('response', $response);
   }
}
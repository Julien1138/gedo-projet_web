<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <title>Gédo</title>
      <link rel="stylesheet" type="text/css" href="style/style.css" />
   </head>
   <body>
      
      <input id="pac-input" class="controls" type="text" placeholder="Rechercher un lieu" />
      <input id="addWateringElement" class="controls" type="image" src="images/addWateringElement-disabled.png" title="Ajouter un pivot ou une rampe" />
      <input id="removeWateringElement" class="controls" type="image" src="images/removeWateringElement-disabled.png" title="Supprimer le pivot ou la rampe déjà présent" />
      <input id="addWateringZone" class="controls" type="image" src="images/addWateringZone-disabled.png" title="Ajouter une zone d'activation du canon" />
      <input id="removeWateringZone" class="controls" type="image" src="images/removeWateringZone-disabled.png" title="Supprimer une zone d'activation du canon" />
      <input id="open" class="controls" type="image" src="image/open-disabled.png" title="Ouvrir une configuration existante" />
      <input id="save" class="controls" type="image" src="image/save-disabled.png" title="Sauvegarder cette configuration" />
      <input id="download" class="controls" type="image" src="image/download-disabled.png" title="Télécharger cette configuration" />
      <p id="infobox" class="controls" type="text"></p>
      <div id="map"></div>
      <div id="dialogOverlay"></div>
      <div id="dialogBox">
         <div id="dialogBoxHead"></div>
         <div id="dialogBoxBody"></div>
         <div id="dialogBoxFoot">
            <input id="dialogBoxOk" class="dialogButton" type="button" value="OK" />
            <input id="dialogBoxCancel" class="dialogButton" type="button" value="Annuler" />
         </div>
      </div>
      
      <script src="javascript/tools.js"></script>
      <script src="javascript/wateringZone.js"></script>
      <script src="javascript/wateringElement.js"></script>
      <script src="javascript/dialog.js"></script>
      <script src="javascript/dataBase.js"></script>
      <script src="javascript/menuButton.js"></script>
      <script src="javascript/app.js" id="appscript"></script>
      <script src="http://api.backendless.com/sdk/js/latest/backendless.js"></script>
      <script async defer
         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDO7V1tQbTvTzpsxJoEb_x9_qkomiB0RIA&libraries=places,geometry&callback=initMap">
      </script>

   </body>
</html>

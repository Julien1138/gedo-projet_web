<?php
namespace GedoFile;

use \ZipArchive;

const temporaryFilesLocation = "files/temp/";
const constantFilesLocation = "files/const/";

class GedoFile
{
   protected $archiveId;

   public function __construct($archiveId)
   {
      $this->archiveId = $archiveId;
   }
   
   public function createConfigFile($json)
   {
      if (!file_exists(temporaryFilesLocation . $this->archiveId))
      {
         mkdir(temporaryFilesLocation . $this->archiveId);
      }
      $configFile = fopen(temporaryFilesLocation . $this->archiveId . "/config.json", "w");
      fwrite($configFile, json_encode($json, JSON_PRETTY_PRINT));
      fclose($configFile);
      
      return file_exists(temporaryFilesLocation . $this->archiveId . "/config.json");;
   }
   
   public function createArchive()
   {
      $zip = new ZipArchive();
      if($zip->open(temporaryFilesLocation . $this->archiveId . "/Gedo.zip", ZIPARCHIVE::OVERWRITE) !== true) {
         return false;
      }
      
      // Ajout des fichiers constants (exe, doc)
      $constFiles = glob(constantFilesLocation . "*.*");
      foreach ($constFiles as $constFile)
      {
         $zip->addFile($constFile, substr($constFile, strlen(constantFilesLocation)));
      }
      
      // Ajout du fichier de config
      $zip->addFile(temporaryFilesLocation . $this->archiveId . "/config.json", "config.json");
      
      $zip->close();
      
      return file_exists(temporaryFilesLocation . $this->archiveId . "/Gedo.zip");
   }
}

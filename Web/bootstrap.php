<?php
const DEFAULT_APP = 'Frontend';

// Si l'application n'est pas valide, on va charger l'application par d�faut qui se chargera de g�n�rer une erreur 404
if (!isset($_GET['app']) || !file_exists(__DIR__.'/../Applications/'.$_GET['app'])) $_GET['app'] = DEFAULT_APP;

// On commence par inclure la classe nous permettant d'enregistrer nos autoload
require __DIR__.'/../Libraries/OCFram/SplClassLoader.php';

// On va ensuite enregistrer les autoloads correspondant � chaque vendor (OCFram, GedoFile...)
$OCFramLoader = new SplClassLoader('OCFram', __DIR__.'/../Libraries');
$OCFramLoader->register();

$GedoFileLoader = new SplClassLoader('GedoFile', __DIR__.'/../Libraries');
$GedoFileLoader->register();

$appLoader = new SplClassLoader('Applications', __DIR__.'/..');
$appLoader->register();

// Il ne nous suffit plus qu'� d�duire le nom de la classe et � l'instancier
$appClass = 'Applications\\'.$_GET['app'].'\\'.$_GET['app'].'Application';

$app = new $appClass;
$app->run();
// Carte
var map;

// Texte d'information pour l'utilisateur
var infobox;

// Boutons
var button = {};

// Action en cours
var actionEnCours;
var userEmailEnCours;
var configNameEnCours;

// Rampe ou pivot
var wateringElement;

// Base de donnée
var dataBase;

function initMap() {
   map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 46.227638, lng: 2.213749},
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.HYBRID,
      streetViewControl: false,
      mapTypeControl: true,
      draggable: true,
      clickableIcons: false,
      mapTypeControlOptions: {
         style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
         position: google.maps.ControlPosition.LEFT_TOP
      }
   });

// Try HTML5 geolocation.
   if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
         lat: position.coords.latitude,
         lng: position.coords.longitude
      };
      map.setCenter(pos);
      map.setZoom(16);
      });
   }

   // Create the search box and link it to the UI element.
   var input = document.getElementById('pac-input');
   var searchBox = new google.maps.places.SearchBox(input);
   map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

   // Create the info box and link it to the UI element.
   infobox = document.getElementById('infobox');
   map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(infobox);

   // Create the addWateringElement button and link it to the UI element.
   button.addWateringElement = new MenuButton('addWateringElement', 'tools', function() {
         actionEnCours = "addingWateringElementCenter";
   });
   
   // Create the removeWateringElement button and link it to the UI element.
   button.removeWateringElement = new MenuButton('removeWateringElement', 'tools', function() {
      ConfirmDialog("Suppression de l'élément", "Êtes vous sûr de vouloir supprimer la rampe ou le pivot déjà présent sur votre carte ?", function() {
         wateringElement.destroy();
         wateringElement = null;
         actionEnCours = "none";
         refreshHMI();
      });
   });

   // Create the addWateringZone button and link it to the UI element.
   button.addWateringZone = new MenuButton('addWateringZone', 'tools', function() {
      actionEnCours = "addingWateringZone";
   });

   // Create the removeWateringZone button and link it to the UI element.
   button.removeWateringZone = new MenuButton('removeWateringZone', 'tools', function() {
      actionEnCours = "removingWateringZone";
   });
   

   // Create the open button and link it to the UI element.
   button.open = new MenuButton('open', 'file', function() {
      LoadConfigNamesDialog(function(userEmail){
         dataBase.LoadConfigNames(userEmail, function(configNames){
            if (configNames == "error"){
               AlertDialog("Erreur", "Impossible de charger la liste de vos configurations. Veuillez vérifier votre adresse email.");
               refreshHMI();
            } else {
               ChooseConfigDialog(configNames, function(configName){
                  dataBase.LoadConfig(userEmail, configName, function(Element){
                     if (Element == "error"){
                        AlertDialog("Erreur", "Impossible de charger la configuration.");
                        refreshHMI();
                     } else {
                        var CenterALatLng = new google.maps.LatLng(Element.centerA.latitude, Element.centerA.longitude);
                        var CenterBLatLng = new google.maps.LatLng(Element.centerB.latitude, Element.centerB.longitude);
                        wateringElement = new WateringElement(CenterALatLng, CenterBLatLng, Element.diameterMeters);
                        for (var i = 0, c = Element.zones.length; i < c; i++) {
                           wateringElement.addWateringZoneToList(Element.zones[i].side1,
                                                                                    Element.zones[i].distanceOrAngle1,
                                                                                    Element.zones[i].side2,
                                                                                    Element.zones[i].distanceOrAngle2);
                        }
                        actionEnCours = "none";
                        refreshHMI();
                        var pos = {
                           lat: (CenterALatLng.lat() + CenterBLatLng.lat()) / 2.0,
                           lng: (CenterALatLng.lng() + CenterBLatLng.lng()) / 2.0
                        };
                        map.setCenter(pos);
                        map.setZoom(16);
                        userEmailEnCours = userEmail
                        configNameEnCours = configName;
                     }
                  });
               });
            }
         });
      }, userEmailEnCours);
   });

   // Create the save button and link it to the UI element.
   button.save = new MenuButton('save', 'file', function() {
      SaveFormDialog(function(userEmail, configName) {
         dataBase.checkConfig(userEmail, configName, function(exists){
            if (exists == "exists") {
               ConfirmDialog("Sauvegarde", "Attention, vous avez déjà sauvegardé une configuration sous ce nom. Voulez-vous la remplacer ?", function(){
                  dataBase.deleteConfig(userEmail, configName, function(result){
                     if (result == "success") {
                        dataBase.saveConfig(userEmail, configName, wateringElement, function(result){
                           if (result == "success") {
                              userEmailEnCours = userEmail
                              configNameEnCours = configName;
                              AlertDialog("Sauvegarde", "Configuration sauvegardée");
                              refreshHMI();
                           }
                        });
                     }
                  });
               });
            } else {
               dataBase.saveConfig(userEmail, configName, wateringElement, function(result){
                  if (result == "success") {
                     userEmailEnCours = userEmail
                     configNameEnCours = configName;
                     AlertDialog("Sauvegarde", "Configuration sauvegardée");
                     refreshHMI();
                  }
               });
            }
         });
      }, userEmailEnCours, configNameEnCours);
   });

   // Create the download button and link it to the UI element.
   button.download = new MenuButton('download', 'file', function() {
      //oXHR.js necessaire ??
         
      var archiveId = '';
      if (isEmpty(userEmailEnCours) || isEmpty(configNameEnCours)) {
         archiveId = Math.floor((Math.random() * 10000) + 1);
         archiveId = archiveId.toString();
      } else {
         archiveId = userEmailEnCours.concat("_", configNameEnCours);
      }
      
      var zones = wateringElement.calcZones();
      
      var xhr = new XMLHttpRequest();
      xhr.open('POST', siteAddress() + '/filegen/generate.html');
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.addEventListener('readystatechange', function() {
         if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.responseText == "OK") {
               DownloadArchiveDialog(archiveId);
            } else {
               AlertDialog("Téléchargement", "Erreur pendant la création de votre archive de configuration.");
            }
            refreshHMI();
         }
      });
      xhr.send('ArchiveId=' + archiveId + '&WateringZones=' + JSON.stringify(zones));
      refreshHMI();
   });
   
   // Create the escape key press event
   document.onkeydown = function(evt) {
      evt = evt || window.event;
      var isEscape = false;
      if ("key" in evt) {
            isEscape = evt.key == "Escape";
      } else {
            isEscape = evt.keyCode == 27;
      }
      if (isEscape) {
         if (actionEnCours == "addingWateringZone"){
            wateringElement.hidePreliminaryWateringZone()
         }
         actionEnCours = "none";
      }
      refreshHMI();
   };


   // Bias the SearchBox results towards current map's viewport.
   map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
      refreshHMI();
   });

   // Listen for the event fired when the user selects a prediction and retrieve
   // more details for that place.
   searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
         return;
      }

      // For each place, center the map
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
         if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
         } else {
            bounds.extend(place.geometry.location);
         }
      });
      map.fitBounds(bounds);
      refreshHMI();
   });
   
   // Gestion des clicks de souris sur la carte
   map.addListener('click', function(mouseEvent){
      if (actionEnCours == 'addingWateringElementCenter') {
         wateringElement = new WateringElement(mouseEvent.latLng, mouseEvent.latLng);
         actionEnCours = 'movingWateringElement';
      } else if (actionEnCours == 'addingWateringZone') {
         if (wateringElement.isResizable(mouseEvent.latLng)) {
            wateringElement.addPreliminaryWateringZoneToList(mouseEvent.latLng);
         }
      } else if (actionEnCours == "removingWateringZone") {
         if (wateringElement.hasRemovableZone(mouseEvent.latLng)) {
            wateringElement.removeZone();
         }
      }
      refreshHMI();
   });
   
   //Gestion des déplacements de souris sur la carte
   map.addListener('mousemove', function(mouseEvent){
      if (actionEnCours == 'none') {   // S'il n'y a pas d'action en cours
         if (!isEmpty(wateringElement) && wateringElement.isMovable(mouseEvent.latLng)) {
            map.setOptions({
               draggableCursor: 'move',
               draggable: false
            });
         } else if (!isEmpty(wateringElement) && wateringElement.hasResizableZone(mouseEvent.latLng)) {
            map.setOptions({
               draggableCursor: wateringElement.reziseZoneCursor(mouseEvent.latLng),
               draggable: false
            });
         } else if (!isEmpty(wateringElement) && wateringElement.hasSelectableZone(mouseEvent.latLng)) {
            map.setOptions({
               draggableCursor: 'pointer',
               draggable: false
            });
         } else if (!isEmpty(wateringElement) && wateringElement.isResizable(mouseEvent.latLng)) {
            map.setOptions({
               draggableCursor: wateringElement.reziseCursor(mouseEvent.latLng),
               draggable: false
            });
         } else {
            map.setOptions({
               draggableCursor: 'url(http://maps.google.com/mapfiles/openhand.cur), move',
               draggable: true
            });
         }
      } else if (actionEnCours == 'movingWateringElement') {
         wateringElement.move(mouseEvent.latLng);
         map.setOptions({
            draggableCursor: 'move',
            draggable: false
         });
      } else if (actionEnCours == 'resizingWateringZone') {
         wateringElement.resizeZone(mouseEvent.latLng);
         map.setOptions({
            draggableCursor: wateringElement.reziseZoneCursor(mouseEvent.latLng),
            draggable: false
         });
      } else if (actionEnCours == 'resizingWateringElement') {
         wateringElement.resize(mouseEvent.latLng);
         map.setOptions({
            draggableCursor: wateringElement.reziseCursor(mouseEvent.latLng),
            draggable: false
         });
      } else if (actionEnCours == 'addingWateringZone') {
         wateringElement.showPreliminaryWateringZone(mouseEvent.latLng);
      } else if (actionEnCours == 'removingWateringZone') {
         if (wateringElement.hasRemovableZone(mouseEvent.latLng)) {
            map.setOptions({
               draggableCursor: 'pointer',
               draggable: false
            });
         } else {
            map.setOptions({
               draggableCursor: 'default',
               draggable: false
            });
         }
      }
   });
   
   //Gestion des appuis du bouton de la souris sur la carte
   map.addListener('mousedown', function(mouseEvent){
      if (actionEnCours == 'none' && !isEmpty(wateringElement)) {
         if (wateringElement.isMovable(mouseEvent.latLng)) {
            actionEnCours = 'movingWateringElement';
         } else if (wateringElement.hasResizableZone(mouseEvent.latLng)) {
            actionEnCours = 'resizingWateringZone';
         } else if (wateringElement.hasSelectableZone(mouseEvent.latLng)) {
            
         } else if (wateringElement.isResizable(mouseEvent.latLng)) {
            actionEnCours = 'resizingWateringElement';
         }
      }
      refreshHMI();
   });
   
   //Gestion des appuis du bouton de la souris sur la carte
   map.addListener('mouseup', function(mouseEvent){
      if ((actionEnCours == 'movingWateringElement') || (actionEnCours == 'resizingWateringElement') || (actionEnCours == 'resizingWateringZone')) {
         actionEnCours = 'none';
      }
      refreshHMI();
   });
   
   wateringElement = null;
   actionEnCours = "none";
   refreshHMI();
   
   dataBase = new DataBase();
   temp = 0;
}

function refreshHMI() {
   // save button
   if (isEmpty(wateringElement)) {
      button.save.disable();
   } else {
      button.save.enable();
   }
   // open Button
   if (isEmpty(wateringElement)) {
      button.open.enable();
   } else {
      button.open.disable();
   }
   // download button
   if (isEmpty(wateringElement)) {
      button.download.disable();
   } else {
      button.download.enable();
   }
   // addWateringElement Button
   if (actionEnCours == 'addingWateringElementCenter') {
      button.addWateringElement.inProgress();
   } else {
      if (!isEmpty(wateringElement)) {
         button.addWateringElement.disable();
      } else {
         button.addWateringElement.enable();
      }
   }
   // removeWateringElement button
   if (!isEmpty(wateringElement)) {
      button.removeWateringElement.enable();
   } else {
      button.removeWateringElement.disable();
   }
   // addWateringZone button
   if (actionEnCours == 'addingWateringZone') {
      button.addWateringZone.inProgress();
   } else {
      if (!isEmpty(wateringElement)) {
         button.addWateringZone.enable();
      } else {
         button.addWateringZone.disable();
      }
   }
   // addWateringZone button
   if (actionEnCours == 'removingWateringZone') {
      button.removeWateringZone.inProgress();
   } else {
      if (!isEmpty(wateringElement) && wateringElement.hasWateringZone()) {
         button.removeWateringZone.enable();
      } else {
         button.removeWateringZone.disable();
      }
   }
   
   // Cursor
   if (actionEnCours == 'none') {
      map.setOptions({
         draggableCursor: 'url(http://maps.google.com/mapfiles/openhand.cur), move',
         draggable: true
      });
   } else if (actionEnCours == 'addingWateringElementCenter') {
      map.setOptions({
         draggableCursor: 'crosshair',
         draggable: false
      });
   } else if (actionEnCours == 'addingWateringZone') {
      map.setOptions({
         draggableCursor: 'default',
         draggable: false
      });
   } else if (actionEnCours == 'removingWateringZone') {
      map.setOptions({
         draggableCursor: 'default',
         draggable: false
      });
   }
   
   // infobox
   if (actionEnCours == 'none') {
      if (isEmpty(wateringElement)) {
         infobox.innerHTML = "Commencez par localiser votre champ.<br>Puis ajoutez votre pivot ou votre rampe.";
      } else {
         infobox.innerHTML = "Vous pouvez :<ul><li>Ajuster la position et la dimension du pivot ou de la rampe</li><li>Ajouter ou modifier une zone d'activation du canon</li><li>Supprimer le pivot ou la rampe</li><li>Sauvegarder cette configuration</li><li>Télécharger cette configuration</li></ul>";
      }
   } else if (actionEnCours == 'addingWateringElementCenter') {
      infobox.innerHTML = "Positionnez le centre du pivot ou de la rampe sur la carte.<br>Re-cliquez sur l'icone pour annuler";
   } else if (actionEnCours == 'addingWateringZone') {
      infobox.innerHTML = "Positionnez une zone d'activation du canon sur le contour extérieur du pivot ou de la rampe.</br>Vous pourrez l'ajuster ensuite.";
   } else if (actionEnCours == 'removingWateringZone') {
      infobox.innerHTML = "Cliquez sur une zone d'activation du canon pour la supprimer.";
   }
}

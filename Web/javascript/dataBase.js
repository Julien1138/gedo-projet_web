//////////////////////////////////
// Classe : DataBase
//////////////////////////////////

function DataBase() {
    var APPLICATION_ID = 'EA9C9286-9177-5F2C-FF41-90D7D8FC9500',
      SECRET_KEY = '5DA86F9E-0A62-B68D-FF37-FF1EAB668D00',
      VERSION = 'v1';
   Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
}

DataBase.prototype.checkConfig = function(userEmail, configName, callback) {
   var query = new Backendless.DataQuery();
   query.condition = "userEmail='" + userEmail + "' and configName='" + configName + "'";
   Backendless.Persistence.of(Config).find(query, new Backendless.Async(
      function(result){
         if (result.data.length == 0) {
            callback("does not exist");
         } else {
            callback("exists");
         }
      },
      function(result){
         callback("error");
      })
   );
}

DataBase.prototype.saveConfig = function(userEmail, configName, wateringElement, callback) {
   var configObject = new Config(userEmail, configName, wateringElement);
   Backendless.Persistence.of(Config).save(configObject, new Backendless.Async(callback("success"), callback("error")));
}

DataBase.prototype.deleteConfig = function(userEmail, configName, callback) {
   var query = new Backendless.DataQuery();
   query.condition = "userEmail='" + userEmail + "' and configName='" + configName + "'";
   query.options = {relations:["unit", "unit.zones"]};
   Backendless.Persistence.of(Config).find(query, new Backendless.Async(
      function(result){
         if (result.data.length == 1) {
         //TODO : Revoir tout �a... Ca laisse des traces dans Backendless (https://backendless.com/documentation/data/js/data_relations_delete.htm)
            for (var i = 0, c = result.data[0].unit.zones.length; i < c; i++) {
               Backendless.Persistence.of(Zone).remove(result.data[0].unit.zones[i], new Backendless.Async(function(){}, callback("error")));
            }
            Backendless.Persistence.of(Unit).remove(result.data[0].unit,   new Backendless.Async(function(){}, callback("error")));
            Backendless.Persistence.of(Config).remove(result.data[0], new Backendless.Async(callback("success"), callback("error")));
         } else {
            callback("error");
         }
      },
      function(result){
         callback("error");
      })
   );
}

DataBase.prototype.LoadConfigNames = function(userEmail, callback) {
   var query = new Backendless.DataQuery();
   query.condition = "userEmail='" + userEmail + "'";
   Backendless.Persistence.of(Config).find(query, new Backendless.Async(
      function(result){
         if (result.data.length == 0) {
            callback("error");
         } else {
            var configCollection = [];
            for (var i = 0, c = result.data.length; i < c; i++) {
               configCollection.push(result.data[i].configName);
            }
            callback(configCollection);
         }
      },
      function(result){
         callback("error");
      })
   );
}

DataBase.prototype.LoadConfig = function(userEmail, configName, callback) {
   var query = new Backendless.DataQuery();
   query.condition = "userEmail='" + userEmail + "' and configName='" + configName + "'";
   query.options = {relations:["unit", "unit.centerA", "unit.centerB", "unit.zones"]};
   Backendless.Persistence.of(Config).find(query, new Backendless.Async(
      function(result){
         if (result.data.length == 1) {
            callback(result.data[0].unit);
         } else {
            callback("error");
         }
      },
      function(result){
         callback("error");
      })
   );
}

function Config(userEmail, configName, wateringElement) {
   this.___class = 'Config';
   this.userEmail = userEmail || "";
   this.configName = configName || "";
   this.unit = new Unit(wateringElement);
}

function Unit(wateringElement) {
   var Unit = wateringElement || {};
   this.___class = 'Unit';
   this.diameterMeters = Unit.diameterMeters || 0.0;
   var CenterALatLng = Unit.centerALatLng || new google.maps.LatLng(0.0, 0.0);
   this.centerA = new GeoPoint();
   this.centerA.latitude =   CenterALatLng.lat() || 0.0;
   this.centerA.longitude =   CenterALatLng.lng() || 0.0;
   var CenterBLatLng = Unit.centerBLatLng || new google.maps.LatLng(0.0, 0.0);
   this.centerB = new GeoPoint();
   this.centerB.latitude =   CenterBLatLng.lat() || 0.0;
   this.centerB.longitude =   CenterBLatLng.lng() || 0.0;
   var ZoneArray = Unit.wateringZoneArray || {};
   if (ZoneArray.length != 0) {
      var zones = [];
      for (var i = 0, c = ZoneArray.length; i < c; i++) {
         var zone = new Zone(ZoneArray[i]);
         zones.push(zone);
      }
      this.zones = zones;
   }
}

function Zone(wateringZone) {
   var Zone = wateringZone || {};
   this.___class = 'Zone';
   this.side1 = Zone.side1 || "";
   this.distanceOrAngle1 = Zone.distanceOrAngle1 || 0.0;
   this.side2 = Zone.side2 || "";
   this.distanceOrAngle2 = Zone.distanceOrAngle2 || 0.0;
}
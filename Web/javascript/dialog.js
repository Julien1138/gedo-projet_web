AlertDialog = function(title, text) {
  ShowDialogBox();
  SetDialogBoxTitle(title);
  SetDialogBoxBody(text);
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = HideDialogBox;
  document.getElementById('dialogBoxCancel').style.display = "none";
}

ConfirmDialog = function(title, text, func) {
  ShowDialogBox();
  SetDialogBoxTitle(title);
  SetDialogBoxBody(text);
  
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = function(){
    HideDialogBox();
    func();
  };
  
  var dialogBoxCancel = document.getElementById('dialogBoxCancel');
  dialogBoxCancel.style.display = "block";
  dialogBoxCancel.onclick = HideDialogBox;
}

SaveFormDialog = function(saveFunction, emailAddress, configName) {
  ShowDialogBox();
  SetDialogBoxTitle("Sauvegarde de la configuration de Gédo");
  var body = '<label for="emailAddress"><b>Veuillez entrer votre adresse e-mail :</b></label>';
  body += '<br><input id="emailAddress" value="' + (emailAddress || "") + '" class="dialogFormInput"><span id="emailAddressTooltip" class="dialogFormTooltip">Votre adresse e-mail n\'est pas valide</span>';
  body += '<br>';
  body += '<br><label for="configName"><b>Veuillez donner un nom à votre configuration :</b>';
  body += "<br><i>Ce nom est unique à chaque configuration Gédo. Il ne doit pas avoir déjà été utilisé pour une autre de vos configuration Gédo. Donnez le nom de votre champ par exemple.</i></label>";
  body += '<br><input id="configName" value="' + (configName || "") + '" class="dialogFormInput"><span id="configNameTooltip" class="dialogFormTooltip">Le nom de la configuration ne doit pas être vide</span>';
  SetDialogBoxBody(body);
  
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = function(){
    if (CheckAllSaveFields()) {
      HideDialogBox();
      saveFunction(document.getElementById('emailAddress').value, document.getElementById('configName').value);
    }
  };
  
  var dialogBoxCancel = document.getElementById('dialogBoxCancel');
  dialogBoxCancel.style.display = "block";
  dialogBoxCancel.onclick = HideDialogBox;
}

LoadConfigNamesDialog = function(loadConfigNamesFunction, emailAddress) {
  ShowDialogBox();
  SetDialogBoxTitle("Chargement d'une configuration existante");
  var body = '<label for="emailAddress"><b>Veuillez entrer votre adresse e-mail :</b></label>';
  body += '<br><input id="emailAddress" value="' + (emailAddress || "") + '" class="dialogFormInput"><span id="emailAddressTooltip" class="dialogFormTooltip">Votre adresse e-mail n\'est pas valide</span>';
  SetDialogBoxBody(body);
  
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = function(){
    if (CheckEmailAddress()) {
      HideDialogBox();
      loadConfigNamesFunction(document.getElementById('emailAddress').value)
    }
  };
  
  var dialogBoxCancel = document.getElementById('dialogBoxCancel');
  dialogBoxCancel.style.display = "block";
  dialogBoxCancel.onclick = HideDialogBox;
}

ChooseConfigDialog = function(configNames, openConfigFunction) {
  ShowDialogBox();
  SetDialogBoxTitle("Chargement d'une configuration existante");
  var body = '<label for="configName"><b>Veuillez selectionner la configuration à charger :</b></label>';
  body += '<select id="configName" class="dialogFormInput">';
  for (var i = 0, c = configNames.length; i < c; i++) {
    body += '<option value="' + configNames[i] + '">' + configNames[i] + '</option>';
  }
  body += '</select>';
  SetDialogBoxBody(body);
  
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = function(){
    HideDialogBox();
    openConfigFunction(document.getElementById('configName').value)
  };
  
  var dialogBoxCancel = document.getElementById('dialogBoxCancel');
  dialogBoxCancel.style.display = "block";
  dialogBoxCancel.onclick = HideDialogBox;
}

DownloadArchiveDialog = function(archiveId) {
  ShowDialogBox();
  SetDialogBoxTitle("Télechargement de l'archive de configuration");
  var body = '<p>Votre fichier de configuration pour votre Gédo est prêt !</br></br>';
  body += 'Veuillez cliquer sur le lien ci-dessous pour le télécharger.</br>';
  body += '<a href="' + siteAddress() + '/files/temp/' + archiveId + '/Gedo.zip"/>Gedo.zip</a>';
  body += '</p>';
  SetDialogBoxBody(body);
  
  var dialogBoxOk = document.getElementById('dialogBoxOk');
  dialogBoxOk.style.display = "block";
  dialogBoxOk.onclick = HideDialogBox;
  document.getElementById('dialogBoxCancel').style.display = "none";
}



ShowDialogBox = function(){
  var dialogOverlay = document.getElementById('dialogOverlay');
  var dialogBox = document.getElementById('dialogBox');
  var winW = window.innerWidth;
  dialogOverlay.style.display = "block";
  dialogBox.style.left = (winW/2) - (550/2)+"px";
  dialogBox.style.top = "100px";
  dialogBox.style.display = "block";
}

HideDialogBox = function(){
  document.getElementById('dialogOverlay').style.display = "none";
  document.getElementById('dialogBox').style.display = "none";
}

SetDialogBoxTitle = function(title){
  document.getElementById('dialogBoxHead').innerHTML = title;
}

SetDialogBoxBody = function(body){
  document.getElementById('dialogBoxBody').innerHTML = body;
}

CheckEmailAddress = function(){
  var emailAddress = document.getElementById('emailAddress');
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9_\.\-]{2,})+\.)+([a-z]{2,4})+$/;
  var emailAddressTooltip = document.getElementById('emailAddressTooltip');
  if (emailAddress.value.length == 0 || !filter.test(emailAddress.value)) {
    emailAddressTooltip.style.display = 'inline-block';
    return false;
  } else {
    emailAddressTooltip.style.display = 'none';
    return true;
  }
}

CheckConfigName = function() {
  var configName = document.getElementById('configName');
  var configNameTooltip = document.getElementById('configNameTooltip');
  if (configName.value.length == 0) {
    configNameTooltip.style.display = 'inline-block';
    return false;
  } else {
    configNameTooltip.style.display = 'none';
    return true;
  }
}

CheckAllSaveFields = function() {
  var check = true;
  if (!CheckEmailAddress()) {
    check = false;
  }
  if (!CheckConfigName()) {
    check = false;
  }
  return check;
}

//////////////////////////////////
// Classe : MenuButton
//////////////////////////////////

function MenuButton(documentId, menu, onClickFunction) {
   this.documentId = documentId;
   this.documentElement = document.getElementById(documentId);
   
   // var controlDiv = document.getElementById('controls');
   // this.documentElement = document.createElement('input');
   // this.documentElement.type = 'image';
   // this.documentElement.style.backgroundColor = '#fff';
   // this.documentElement.style.border = '2px solid #fff';
   // this.documentElement.style.borderRadius = '3px';
   // this.documentElement.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
   // this.documentElement.style.cursor = 'pointer';
   // this.documentElement.style.marginTop = '22px';
   // this.documentElement.style.marginBottom = '22px';
   // this.documentElement.style.textAlign = 'center';
   // this.documentElement.title = 'Click to recenter the map';
   // controlDiv.appendChild(this.documentElement);
   
   if (menu == 'file') {
      map.controls[google.maps.ControlPosition.TOP_CENTER].push(this.documentElement);
   } else if (menu == 'tools') {
      map.controls[google.maps.ControlPosition.RIGHT_TOP].push(this.documentElement);
   }
   this.status = 'disabled';
   this.onClickFunction = onClickFunction;
   this.documentElement.addEventListener('click', this, false);
}

MenuButton.prototype.enable = function () {
   this.status = 'enabled';
   this.invalidate();
}

MenuButton.prototype.disable = function () {
   this.status = 'disabled';
   this.invalidate();
}

MenuButton.prototype.inProgress = function () {
   this.status = 'inProgress';
   this.invalidate();
}

MenuButton.prototype.invalidate = function () {
   this.documentElement.src = "images/" + this.documentId + "-" + this.status + ".png"
}

MenuButton.prototype.handleEvent = function(event) {
   switch(event.type) {
      case 'click':
         this.onClick();
         break;
   }
}

MenuButton.prototype.onClick = function () {
   switch(this.status) {
      case 'enabled':
         this.onClickFunction();
         break;
      case 'inProgress':
         actionEnCours = "none";
         break;
   }
   refreshHMI();
}


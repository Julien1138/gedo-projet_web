/* ** cartouche ********************************************************************* */
/* Script complet de gestion d'une requ�te de type XMLHttpRequest                     */
/* Par S�bastien de la Marck (aka Thunderseb)                                         */
/* http://fr.openclassrooms.com/informatique/cours/ajax-et-l-echange-de-donnees-en-javascript/l-objet-xmlhttprequest-1 */
/* ********************************************************************************** */

function getXMLHttpRequest() {
   var xhr = null;
   
   if (window.XMLHttpRequest || window.ActiveXObject) {
      if (window.ActiveXObject) {
         try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
         } catch(e) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
         }
      } else {
         xhr = new XMLHttpRequest(); 
      }
   } else {
      alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
      return null;
   }
   
   return xhr;
}

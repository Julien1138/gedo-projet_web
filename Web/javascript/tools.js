//////////////////////////////////
// Fonctions de dessin
//////////////////////////////////
var EarthRadiusMeters = 6378137.0; // meters
function drawArc(center, initialBearing, finalBearing, radius) { 
   var d2r = Math.PI / 180;    // degrees to radians 
   var r2d = 180 / Math.PI;    // radians to degrees 

   // var points = 32;

   // find the raidus in lat/lon 
   var rlat = (radius / EarthRadiusMeters) * r2d; 
   var rlng = rlat / Math.cos(center.lat() * d2r); 

   var extp = new Array();

   if (initialBearing > finalBearing) finalBearing += 360;
   var deltaBearing = finalBearing - initialBearing;
   var points = Math.round(deltaBearing/5.0);   // Un point tous les 5 degr�s.
   points = points == 0 ? 1 : points;   // Au moins 2 points
   deltaBearing = deltaBearing/points;
   for (var i=0; (i < points+1); i++)
   {
      extp.push(google.maps.geometry.spherical.computeOffset(center, radius, initialBearing + i*deltaBearing));
   }
   return extp;
}

//////////////////////////////////
// Outils javascript
//////////////////////////////////
function isEmpty(obj) {

    // null and undefined are "empty"
   if (obj == null) return true;

   // Assume if it has a length property with a non-zero value
   // that that property is correct.
   if (obj.length > 0)      return false;
   if (obj.length === 0)   return true;

   // Otherwise, does it have any properties of its own?
   // Note that this doesn't handle
   // toString and valueOf enumeration bugs in IE < 9
   for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
   }

   return true;
}

function siteAddress() {
   var script = document.getElementById('appscript');
   var address = script.src;
   address = address.substring(0, address.lastIndexOf('/'));
   address = address.substring(0, address.lastIndexOf('/'));
   return address;
}

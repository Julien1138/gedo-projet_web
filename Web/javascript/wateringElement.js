//////////////////////////////////
// Classe : WateringElement
//////////////////////////////////

var angularHalfWidth = 10;
var lineHalfWidth = 0.05;

function WateringElement(centerALatLng, centerBLatLng, diameterMeters) {
   this.outerLine = new google.maps.Polygon({
      map: map,
      strokeColor: "#3498db",
      strokeOpacity: 0.8,
      strokeWeight: 3,
      fillColor: "#3498db",
      fillOpacity: 0.2,
      zIndex: 0,
      clickable: false
   });
   this.centerLine = new google.maps.Polyline({
      map: map,
      geodesic: true,
      strokeColor: "#3498db",
      strokeOpacity: 0.8,
      strokeWeight: 5,
      zIndex: 0,
      clickable: false
   });
   this.centerALine = new google.maps.Polyline({
      map: map,
      strokeColor: "#3498db",
      strokeOpacity: 0.8,
      strokeWeight: 10,
      zIndex: 0,
      clickable: false
   });
   this.centerBLine = new google.maps.Polyline({
      map: map,
      strokeColor: "#3498db",
      strokeOpacity: 0.8,
      strokeWeight: 10,
      zIndex: 0,
      clickable: false
   });
   this.preliminaryWateringZone = new WateringZone("A", 0, "A", 0, true);
   
   this.diameterMeters = diameterMeters || 200;
   this.centerALatLng = centerALatLng;
   this.centerBLatLng = centerBLatLng;
   this.angleAB = google.maps.geometry.spherical.computeHeading(this.centerALatLng, this.centerBLatLng);
   this.isMoving = "PointB";
   this.wateringZoneArray = [];
   this.selectedZone = -1;
   this.drawLines();
}

WateringElement.prototype.destroy = function () {
   this.outerLine.setMap(null);
   this.outerLine = null;
   this.centerLine.setMap(null);
   this.centerLine = null;
   this.centerALine.setMap(null);
   this.centerALine = null;
   this.centerBLine.setMap(null);
   this.centerBLine = null;
   while (this.wateringZoneArray.length != 0) {
      this.wateringZoneArray[0].destroy();
      this.wateringZoneArray.shift();
   }
}

WateringElement.prototype.hasWateringZone = function () {
   return (this.wateringZoneArray.length != 0);
}

WateringElement.prototype.isMovable = function (latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.centerALine, 0.0001)){
      if (this.centerALatLng.equals(this.centerBLatLng))
         this.isMoving = "BothPoints";
      else
         this.isMoving = "PointA";
      return true;
   }
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.centerBLine, 0.0001)){
      if (this.centerALatLng.equals(this.centerBLatLng))
         this.isMoving = "BothPoints";
      else
         this.isMoving = "PointB";
      return true;
   }
   
   return false;
}

WateringElement.prototype.isResizable = function (latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.outerLine, 0.0001)){
      return true;
   }
   
   return false;
}

WateringElement.prototype.save = function (userEmail, configName) {
   function Element(args) {
      
   }
   var test = Backendless.Persistence.of(WateringElement).save(this);
   if (test == null) {
      alert("null");
   }
}

WateringElement.prototype.hasResizableZone = function (latLng) {
   for (var i = 0, c = this.wateringZoneArray.length; i < c; i++) {
      if (this.wateringZoneArray[i].isResizable(latLng)) {
         this.selectedZone = i;
         return true;
      }
   }
   this.selectedZone = -1;
   return false;
}

WateringElement.prototype.hasSelectableZone = function (latLng) {
   for (var i = 0, c = this.wateringZoneArray.length; i < c; i++) {
      if (this.wateringZoneArray[i].isSelectable(latLng)) {
         this.selectedZone = i;
         return true;
      }
   }
   this.selectedZone = -1;
   return false;
}

WateringElement.prototype.hasRemovableZone = function (latLng) {
   for (var i = 0, c = this.wateringZoneArray.length; i < c; i++) {
      if (this.wateringZoneArray[i].isRemovable(latLng)) {
         this.selectedZone = i;
         return true;
      }
   }
   this.selectedZone = -1;
   return false;
}

WateringElement.prototype.reziseCursor = function (latLng) {
   var angleA = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng) - this.angleAB;
   while (angleA < -180)
      angleA += 360;
   while (angleA > 180)
      angleA -= 360;
   var angleB = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng) - this.angleAB + 180;
   while (angleB < -180)
      angleB += 360;
   while (angleB > 180)
      angleB -= 360;
   
   if (Math.abs(angleA) <= 90 && Math.abs(angleB) <= 90) {   // Sur les lignes droites
      angleAB = this.angleAB;
      if (angleAB < 0)
         angleAB +=180;
      if (angleAB < 22.5 || angleAB >= 157.5)
         return "e-resize";
      if (angleAB >= 22.5 && angleAB < 67.5)
         return "se-resize";
      if (angleAB >= 67.5 && angleAB < 112.5)
         return "s-resize";
      if (angleAB >= 112.5 && angleAB < 157.5)
         return "sw-resize";
      return "e-resize";
   } else {
      if (Math.abs(angleA) > 90) {   // c�t� A
         var heading = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng);
      } else if (Math.abs(angleB) > 90) {   // c�t� B
         var heading = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng);
      }
      if (heading < 0)
         heading +=180;
      if (heading < 22.5 || heading >= 157.5)
         return "s-resize";
      if (heading >= 22.5 && heading < 67.5)
         return "sw-resize";
      if (heading >= 67.5 && heading < 112.5)
         return "e-resize";
      if (heading >= 112.5 && heading < 157.5)
         return "se-resize";
      return "e-resize";
   }
   return "e-resize";
}

WateringElement.prototype.reziseZoneCursor = function (latLng) {
   var angleA = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng) - this.angleAB;
   while (angleA < -180)
      angleA += 360;
   while (angleA > 180)
      angleA -= 360;
   var angleB = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng) - this.angleAB + 180;
   while (angleB < -180)
      angleB += 360;
   while (angleB > 180)
      angleB -= 360;
   
   if (Math.abs(angleA) <= 90 && Math.abs(angleB) <= 90) {   // Sur les lignes droites
      angleAB = this.angleAB;
      if (angleAB < 0)
         angleAB +=180;
      if (angleAB < 22.5 || angleAB >= 157.5)
         return "s-resize";
      if (angleAB >= 22.5 && angleAB < 67.5)
         return "sw-resize";
      if (angleAB >= 67.5 && angleAB < 112.5)
         return "e-resize";
      if (angleAB >= 112.5 && angleAB < 157.5)
         return "se-resize";
   } else {
      if (Math.abs(angleA) > 90) {   // c�t� A
         var heading = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng);
      } else if (Math.abs(angleB) > 90) {   // c�t� B
         var heading = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng);
      }
      if (heading < 0)
         heading +=180;
      if (heading < 22.5 || heading >= 157.5)
         return "e-resize";
      if (heading >= 22.5 && heading < 67.5)
         return "se-resize";
      if (heading >= 67.5 && heading < 112.5)
         return "s-resize";
      if (heading >= 112.5 && heading < 157.5)
         return "sw-resize";
      return "e-resize";
   }
   return "e-resize";
}

WateringElement.prototype.move = function(latLng) {
   if (this.isMoving == "BothPoints") {
      this.centerALatLng = latLng;
      this.centerBLatLng = latLng;
   } else if (this.isMoving == "PointA") {
      if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.centerBLine, 0.0005))
         this.centerALatLng = this.centerBLatLng;
      else
         this.centerALatLng = latLng;
   } else if (this.isMoving == "PointB") {
      if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.centerALine, 0.0005))
         this.centerBLatLng = this.centerALatLng;
      else
         this.centerBLatLng = latLng;
   }
   this.angleAB = google.maps.geometry.spherical.computeHeading(this.centerALatLng, this.centerBLatLng);
   this.drawLines();
}

WateringElement.prototype.resize = function(latLng) {
   var angleA = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng) - this.angleAB;
   while (angleA < -180)
      angleA += 360;
   while (angleA > 180)
      angleA -= 360;
   var angleB = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng) - this.angleAB + 180;
   while (angleB < -180)
      angleB += 360;
   while (angleB > 180)
      angleB -= 360;
   
   if (Math.abs(angleA) <= 90 && Math.abs(angleB) <= 90) {   // Sur les lignes droites
      this.diameterMeters = Math.abs(Math.sin(angleA * Math.PI / 180)) * google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, latLng);
   } else if (Math.abs(angleA) > 90) {   // c�t� A
      this.diameterMeters = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, latLng);
   } else if (Math.abs(angleB) > 90) {   // c�t� B
      this.diameterMeters = google.maps.geometry.spherical.computeDistanceBetween(this.centerBLatLng, latLng);
   }
   
   this.drawLines();
}

WateringElement.prototype.resizeZone = function(latLng) {
      var headingA = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng);
      var angleA = headingA - this.angleAB;
      while (angleA < -180)
         angleA += 360;
      while (angleA > 180)
         angleA -= 360;
      var headingB = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng);
      var angleB = headingB - this.angleAB + 180;
      while (angleB < -180)
         angleB += 360;
      while (angleB > 180)
         angleB -= 360;
      
      if (angleA > 90 || angleA < -90) {
         this.wateringZoneArray[this.selectedZone].resize("A", angleA > 0 ? angleA - 90 : angleA + 270);
      } else if (angleB > 90 || angleB < -90) {
         this.wateringZoneArray[this.selectedZone].resize("B", angleB > 0 ? angleB - 90 : angleB + 270);
      } else if (angleA >= -90 && angleA < 0 && angleB <= 90 && angleB > 0) {
         var dist = Math.cos(angleA * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.wateringZoneArray[this.selectedZone].resize("A+", dist/distMax);
      } else if (angleB >= -90 && angleB < 0 && angleA <= 90 && angleA > 0) {
         var dist = Math.cos(angleB * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerBLatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.wateringZoneArray[this.selectedZone].resize("B+", dist/distMax);
      }
   
   
   this.drawLines();
}

WateringElement.prototype.removeZone = function() {
   this.wateringZoneArray[this.selectedZone].destroy();
   this.wateringZoneArray.splice(this.selectedZone, 1);
   this.drawLines();
}

WateringElement.prototype.showPreliminaryWateringZone = function(latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.outerLine, 0.0002)) {
      var headingA = google.maps.geometry.spherical.computeHeading(this.centerALatLng, latLng);
      var angleA = headingA - this.angleAB;
      while (angleA < -180)
         angleA += 360;
      while (angleA > 180)
         angleA -= 360;
      var headingB = google.maps.geometry.spherical.computeHeading(this.centerBLatLng, latLng);
      var angleB = headingB - this.angleAB + 180;
      while (angleB < -180)
         angleB += 360;
      while (angleB > 180)
         angleB -= 360;
      
      // SetPoint1
      if (angleA > (90 + angularHalfWidth) || angleA < (-90 + angularHalfWidth)) {
         this.preliminaryWateringZone.setPoint1("A", angleA > 0 ? angleA - (90 + angularHalfWidth) : angleA + (270 - angularHalfWidth));
      } else if (angleB > (90 + angularHalfWidth) || angleB < (-90 + angularHalfWidth)) {
         this.preliminaryWateringZone.setPoint1("B", angleB > 0 ? angleB - (90 + angularHalfWidth) : angleB + (270 - angularHalfWidth));
      } else if (angleA >= (-90 + angularHalfWidth) && angleA < 0 && angleB <= (90 + angularHalfWidth) && angleB > 0) {
         var dist = Math.cos(angleA * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.preliminaryWateringZone.setPoint1("A+", dist/distMax - lineHalfWidth);
      } else if (angleB >= (-90 + angularHalfWidth) && angleB < 0 && angleA <= (90 + angularHalfWidth) && angleA > 0) {
         var dist = Math.cos(angleB * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerBLatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.preliminaryWateringZone.setPoint1("B+", dist/distMax - lineHalfWidth);
      }
      
      // SetPoint2
      if (angleA > (90 - angularHalfWidth) || angleA < (-90 - angularHalfWidth)) {
         this.preliminaryWateringZone.setPoint2("A", angleA > 0 ? angleA - (90 - angularHalfWidth) : angleA + (270 + angularHalfWidth));
      } else if (angleB > (90 - angularHalfWidth) || angleB < (-90 - angularHalfWidth)) {
         this.preliminaryWateringZone.setPoint2("B", angleB > 0 ? angleB - (90 - angularHalfWidth) : angleB + (270 + angularHalfWidth));
      } else if (angleA >= (-90 - angularHalfWidth) && angleA < 0 && angleB <= (90 - angularHalfWidth) && angleB > 0) {
         var dist = Math.cos(angleA * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.preliminaryWateringZone.setPoint2("A+", dist/distMax + lineHalfWidth);
      } else if (angleB >= (-90 - angularHalfWidth) && angleB < 0 && angleA <= (90 - angularHalfWidth) && angleA > 0) {
         var dist = Math.cos(angleB * Math.PI / 180) * google.maps.geometry.spherical.computeDistanceBetween(this.centerBLatLng, latLng);
         var distMax = google.maps.geometry.spherical.computeDistanceBetween(this.centerALatLng, this.centerBLatLng)
         this.preliminaryWateringZone.setPoint2("B+", dist/distMax + lineHalfWidth);
      }
      
      this.preliminaryWateringZone.drawLine(this.centerALatLng, this.centerBLatLng, this.diameterMeters);
      this.preliminaryWateringZone.show();
   } else {
      this.preliminaryWateringZone.hide();
   }
}

WateringElement.prototype.hidePreliminaryWateringZone = function() {
   this.preliminaryWateringZone.hide();
}

WateringElement.prototype.addPreliminaryWateringZoneToList = function() {
   var zone = new WateringZone(this.preliminaryWateringZone.side1,
                               this.preliminaryWateringZone.distanceOrAngle1,
                               this.preliminaryWateringZone.side2,
                               this.preliminaryWateringZone.distanceOrAngle2,
                               false);
   this.wateringZoneArray.push(zone);
   this.preliminaryWateringZone.hide();
   this.drawLines();
}

WateringElement.prototype.addWateringZoneToList = function(side1, distanceOrAngle1, side2, distanceOrAngle2) {
   var zone = new WateringZone(side1,
                               distanceOrAngle1,
                               side2,
                               distanceOrAngle2,
                               false);
   this.wateringZoneArray.push(zone);
   this.preliminaryWateringZone.hide();
   this.drawLines();
}

WateringElement.prototype.drawLines = function() {
   this.outerLine.setPath(drawArc(this.centerALatLng, this.angleAB + 90.0, this.angleAB + 270.0 , this.diameterMeters).concat(drawArc(this.centerBLatLng, this.angleAB - 90.0, this.angleAB + 90.0 , this.diameterMeters)));
   
   this.centerALine.setPath([this.centerALatLng, this.centerALatLng]);
   this.centerBLine.setPath([this.centerBLatLng, this.centerBLatLng]);
   this.centerLine.setPath([this.centerALatLng, this.centerBLatLng]);
   
   for (var i = 0, c = this.wateringZoneArray.length; i < c; i++) {
      this.wateringZoneArray[i].drawLine(this.centerALatLng, this.centerBLatLng, this.diameterMeters);
      //this.wateringZoneArray[i].calcZone(this.centerALatLng, this.centerBLatLng, this.diameterMeters);
   }
}

WateringElement.prototype.calcZones = function () {
   var points = [];
   for (var i = 0, c = this.wateringZoneArray.length; i < c; i++) {
      points = points.concat(this.wateringZoneArray[i].calcZone(this.centerALatLng, this.centerBLatLng, this.diameterMeters));
   }
   
   var zones = {};
   for (var i = 0, c = points.length; i < c; i+=4) {
      var zone = [points[i], points[i+1], points[i+2], points[i+3]];
      zones["zone".concat((i/4).toString())] = zone;
   }
   return zones;
}

WateringElement.prototype.debug = function (latLng) {
   if (!isEmpty(this.preliminaryWateringZone)) {
      return this.preliminaryWateringZone.debug();
   }
   return "debug";
}

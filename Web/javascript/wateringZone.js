//////////////////////////////////
// Classe : WateringZone
//////////////////////////////////

function WateringZone(side1, distanceOrAngle1, side2, distanceOrAngle2, preliminary) {
   if (preliminary) {
      this.zoneLine = new google.maps.Polyline({
         map: map,
         strokeColor: "#27ae60",
         strokeOpacity: 1.0,
         strokeWeight: 5,
         zIndex: 10,
         clickable: false
      });
   } else {
      this.zoneLine = new google.maps.Polyline({
         map: map,
         strokeColor: "#2ecc71",
         strokeOpacity: 1.0,
         strokeWeight: 5,
         zIndex: 10,
         clickable: false
      });
   }
   this.point1 = new google.maps.Polyline({
      strokeColor: "#000000",
      strokeOpacity: 1.0,
      strokeWeight: 3,
      zIndex: 10,
      clickable: false
   });
   this.point2 = new google.maps.Polyline({
      strokeColor: "#000000",
      strokeOpacity: 1.0,
      strokeWeight: 3,
      zIndex: 10,
      clickable: false
   });
   // this.zone = new google.maps.Polyline({
      // map: map,
      // strokeColor: "#ff00ff",
      // strokeOpacity: 0.5,
      // strokeWeight: 3,
      // zIndex: 10,
      // clickable: false
   // });
   
   this.side1 = side1;
   this.distanceOrAngle1 = distanceOrAngle1; // Entre 0 et 1 ou entre 0 et 360
   this.side2 = side2;
   this.distanceOrAngle2 = distanceOrAngle2; // Entre 0 et 1 ou entre 0 et 360
   this.isMoving = "";
}

WateringZone.prototype.destroy = function () {
   this.zoneLine.setMap(null);
   this.zoneLine = null;
   this.point1.setMap(null);
   this.point1 = null;
   this.point2.setMap(null);
   this.point2 = null;
}

WateringZone.prototype.setPoint1 = function (side, distanceOrAngle) {
   this.side1 = side;
   this.distanceOrAngle1 = distanceOrAngle;
}

WateringZone.prototype.setPoint2 = function (side, distanceOrAngle) {
   this.side2 = side;
   this.distanceOrAngle2 = distanceOrAngle;
}

WateringZone.prototype.isSelectable = function (latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.zoneLine, 0.0001)){
      this.point1.setMap(map);
      this.point2.setMap(map);
      return true;
   }
   this.point1.setMap(null);
   this.point2.setMap(null);
   return false;
}

WateringZone.prototype.isResizable = function (latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.point1, 0.0001)){
      this.point1.setMap(map);
      this.point2.setMap(map);
      this.isMoving = "Point1";
      return true;
   }
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.point2, 0.0001)){
      this.point1.setMap(map);
      this.point2.setMap(map);
      this.isMoving = "Point2";
      return true;
   }
   this.point1.setMap(null);
   this.point2.setMap(null);
   this.isMoving = "";
   return false;
}

WateringZone.prototype.isRemovable = function (latLng) {
   if (google.maps.geometry.poly.isLocationOnEdge(latLng, this.zoneLine, 0.0001)){
      this.zoneLine.setOptions({strokeColor: "#e74c3c"});
      return true;
   }
   this.zoneLine.setOptions({strokeColor: "#2ecc71"});
   return false;
}

WateringZone.prototype.resize = function (side, distanceOrAngle) {
   if (this.isMoving == "Point1") {
      this.setPoint1(side, distanceOrAngle);
   } else if (this.isMoving == "Point2") {
      this.setPoint2(side, distanceOrAngle);
   }
}

WateringZone.prototype.drawLine = function(centerALatLng, centerBLatLng, diameterMeters) {
   var angleAB = google.maps.geometry.spherical.computeHeading(centerALatLng, centerBLatLng);
   var angleStartA = angleAB + 90.0;    // StartA = EndB
   var angleStartB = angleAB - 90.0;    // StartB = EndA
   
   if (this.side1 == "A") {
      this.point1.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartA + this.distanceOrAngle1, diameterMeters));
      if (this.side2 == "A" && this.distanceOrAngle1 < this.distanceOrAngle2) {
         this.zoneLine.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartA + this.distanceOrAngle2, diameterMeters));
         this.point2.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle2, angleStartA + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "A+") {
         var pathA = drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartB, diameterMeters);
         var startLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartB);
         var endLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartB);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath(pathA.concat(endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "B") {
         var pathA = drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartB, diameterMeters);
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartB + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath(pathA.concat(pathB));
         this.point2.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle2, angleStartB + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "B+") {
         var pathA = drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartB, diameterMeters);
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var startLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartA);
         var endLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartA);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath(pathA.concat(pathB, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "A") {
         var pathAStart = drawArc(centerALatLng, angleStartA + this.distanceOrAngle1, angleStartB, diameterMeters);
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var pathAEnd = drawArc(centerALatLng, angleStartA, angleStartA + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath(pathAStart.concat(pathB, pathAEnd));
         this.point2.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle2, angleStartA + this.distanceOrAngle2, diameterMeters));
      }
   } else if (this.side1 == "B") {
      this.point1.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartB + this.distanceOrAngle1, diameterMeters));
      if (this.side2 == "B" && this.distanceOrAngle1 < this.distanceOrAngle2) {
         this.zoneLine.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartB + this.distanceOrAngle2, diameterMeters));
         this.point2.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle2, angleStartB + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "B+") {
         var pathB = drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartA, diameterMeters);
         var startLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartA);
         var endLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartA);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath(pathB.concat(endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "A") {
         var pathB = drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartA, diameterMeters);
         var pathA = drawArc(centerALatLng, angleStartA, angleStartA + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath(pathB.concat(pathA));
         this.point2.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle2, angleStartA + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "A+") {
         var pathB = drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartA, diameterMeters);
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var startLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartB);
         var endLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartB);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath(pathB.concat(pathA, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "B") {
         var pathBStart = drawArc(centerBLatLng, angleStartB + this.distanceOrAngle1, angleStartA, diameterMeters);
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var pathBEnd = drawArc(centerBLatLng, angleStartB, angleStartB + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath(pathBStart.concat(pathA, pathBEnd));
         this.point2.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle2, angleStartB + this.distanceOrAngle2, diameterMeters));
      }
   } else if (this.side1 == "A+") {
      var startLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartB);
      var endLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartB);
      var startPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle1);
      this.point1.setPath([startPoint, startPoint]);
      if (this.side2 == "A+" && this.distanceOrAngle1 < this.distanceOrAngle2) {
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint, endPoint]);
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "B") {
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartB + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath([startPoint].concat(pathB));
         this.point2.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle2, angleStartB + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "B+") {
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var startLatLngB = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartA);
         var endLatLngB = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartA);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLngB, endLatLngB, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint].concat(pathB, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "A") {
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var pathA = drawArc(centerALatLng, angleStartA, angleStartA + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath([startPoint].concat(pathB, pathA));
         this.point2.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle2, angleStartA + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "A+") {
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint].concat(pathB, pathA, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      }
   } else if (this.side1 == "B+") {
      var startLatLng = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartA);
      var endLatLng = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartA);
      var startPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle1);
      this.point1.setPath([startPoint, startPoint]);
      if (this.side2 == "B+" && this.distanceOrAngle1 < this.distanceOrAngle2) {
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint, endPoint]);
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "A") {
         var pathA = drawArc(centerALatLng, angleStartA, angleStartA + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath([startPoint].concat(pathA));
         this.point2.setPath(drawArc(centerALatLng, angleStartA + this.distanceOrAngle2, angleStartA + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "A+") {
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var startLatLngA = google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters, angleStartB);
         var endLatLngA = google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters, angleStartB);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLngA, endLatLngA, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint].concat(pathA, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      } else if (this.side2 == "B") {
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartB + this.distanceOrAngle2, diameterMeters);
         this.zoneLine.setPath([startPoint].concat(pathA, pathB));
         this.point2.setPath(drawArc(centerBLatLng, angleStartB + this.distanceOrAngle2, angleStartB + this.distanceOrAngle2, diameterMeters));
      } else if (this.side2 == "B+") {
         var pathA = drawArc(centerALatLng, angleStartA, angleStartB, diameterMeters);
         var pathB = drawArc(centerBLatLng, angleStartB, angleStartA, diameterMeters);
         var endPoint = google.maps.geometry.spherical.interpolate(startLatLng, endLatLng, this.distanceOrAngle2);
         this.zoneLine.setPath([startPoint].concat(pathA, pathB, endPoint));
         this.point2.setPath([endPoint, endPoint]);
      }
   }
}

WateringZone.prototype.show = function () {
   this.zoneLine.setMap(map);
}

WateringZone.prototype.hide = function () {
   this.zoneLine.setMap(null);
}

WateringZone.prototype.showPoints = function () {
   this.point1.setMap(map);
   this.point2.setMap(map);
}

WateringZone.prototype.hidePoints = function () {
   this.point1.setMap(null);
   this.point2.setMap(null);
}

WateringZone.prototype.calcZone = function (centerALatLng, centerBLatLng, diameterMeters) {
   var angleAB = google.maps.geometry.spherical.computeHeading(centerALatLng, centerBLatLng);
   var angleStartA = angleAB + 90.0;    // StartA = EndB
   var angleStartB = angleAB - 90.0;    // StartB = EndA
   var distAB = google.maps.geometry.spherical.computeDistanceBetween(centerALatLng, centerBLatLng);
   var points = [];
   
   //D�but de la zone
   if (this.side1 == "A") {
      points.push(centerALatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + this.distanceOrAngle1));
      if (this.side2 != "A" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on fini dans une autre zone
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + (this.distanceOrAngle1 + 180.0)/2.0));
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartB));
      }
   } else if (this.side1 == "A+") {
      var point = google.maps.geometry.spherical.computeOffset(centerALatLng, distAB*this.distanceOrAngle1, angleAB)
      points.push(point);
      points.push(google.maps.geometry.spherical.computeOffset(point, diameterMeters*2.0, angleStartB));
      if (this.side2 != "A+" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on fini dans une autre zone
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB));
         points.push(centerBLatLng);
      }
   } else if (this.side1 == "B") {
      points.push(centerBLatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + this.distanceOrAngle1));
      if (this.side2 != "B" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on fini dans une autre zone
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + (this.distanceOrAngle1 + 180.0)/2.0));
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartA));
      }
   } else if (this.side1 == "B+") {
      var point = google.maps.geometry.spherical.computeOffset(centerBLatLng, distAB*this.distanceOrAngle1, angleAB+180.0)
      points.push(point);
      points.push(google.maps.geometry.spherical.computeOffset(point, diameterMeters*2.0, angleStartA));
      if (this.side2 != "B+" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on fini dans une autre zone
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA));
         points.push(centerALatLng);
      }
   }
   
   // Zone A complete
   if ((this.side1 == this.side2 && this.distanceOrAngle1 > this.distanceOrAngle2 && this.side1 != "A") || // Si on termine et on commence dans la m�me zone (sauf A) en pasant par toutes les autres (dont A)
       (this.side1 == "B" && this.side2 == "A+") || (this.side1 == "B+" && (this.side2 == "A+" || this.side2 == "B"))) { // Les autres cas o� on passe par A
      points.push(centerALatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA));
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + 90.0));
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartB));
   }
   
   // Zone A+ complete
   if ((this.side1 == this.side2 && this.distanceOrAngle1 > this.distanceOrAngle2 && this.side1 != "A+") || // Si on termine et on commence dans la m�me zone (sauf A+) en pasant par toutes les autres (dont A+)
       (this.side1 == "B+" && this.side2 == "B") || (this.side1 == "A" && (this.side2 == "B" || this.side2 == "B+"))) { // Les autres cas o� on passe par A+
      points.push(centerALatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartB));
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB));
      points.push(centerBLatLng);
   }
   
   // Zone B complete
   if ((this.side1 == this.side2 && this.distanceOrAngle1 > this.distanceOrAngle2 && this.side1 != "B") || // Si on termine et on commence dans la m�me zone (sauf B) en pasant par toutes les autres (dont B)
       (this.side1 == "A" && this.side2 == "B+") || (this.side1 == "A+" && (this.side2 == "B+" || this.side2 == "A"))) { // Les autres cas o� on passe par B
      points.push(centerBLatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB));
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + 90.0));
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartA));
   }
   
   // Zone B+ complete
   if ((this.side1 == this.side2 && this.distanceOrAngle1 > this.distanceOrAngle2 && this.side1 != "B+") || // Si on termine et on commence dans la m�me zone (sauf B+) en pasant par toutes les autres (dont B+)
       (this.side1 == "A+" && this.side2 == "A") || (this.side1 == "B" && (this.side2 == "A" || this.side2 == "A+"))) { // Les autres cas o� on passe par A+
      points.push(centerBLatLng);
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartA));
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA));
      points.push(centerALatLng);
   }
   
   
   //Fin de la zone
   if (this.side2 == "A") {
      if (this.side1 != "A" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on vient d'une autre zone
         points.push(centerALatLng);
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA));
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + this.distanceOrAngle2/2.0));
      } else {
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + (this.distanceOrAngle1 + this.distanceOrAngle2)/2.0));
      }
      points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartA + this.distanceOrAngle2));
   } else if (this.side2 == "A+") {
      if (this.side1 != "A+" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on vient d'une autre zone
         points.push(centerALatLng);
         points.push(google.maps.geometry.spherical.computeOffset(centerALatLng, diameterMeters*2.0, angleStartB));
      }
      var point = google.maps.geometry.spherical.computeOffset(centerALatLng, distAB*this.distanceOrAngle2, angleAB)
      points.push(google.maps.geometry.spherical.computeOffset(point, diameterMeters*2.0, angleStartB));
      points.push(point);
   } else if (this.side2 == "B") {
      if (this.side1 != "B" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on vient d'une autre zone
         points.push(centerBLatLng);
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB));
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + this.distanceOrAngle2/2.0));
      } else {
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + (this.distanceOrAngle1 + this.distanceOrAngle2)/2.0));
      }
      points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartB + this.distanceOrAngle2));
   } else if (this.side2 == "B+") {
      if (this.side1 != "B+" || this.distanceOrAngle1 > this.distanceOrAngle2) { // Si on vient d'une autre zone
         points.push(centerBLatLng);
         points.push(google.maps.geometry.spherical.computeOffset(centerBLatLng, diameterMeters*2.0, angleStartA));
      }
      var point = google.maps.geometry.spherical.computeOffset(centerBLatLng, distAB*this.distanceOrAngle2, angleAB+180)
      points.push(google.maps.geometry.spherical.computeOffset(point, diameterMeters*2.0, angleStartA));
      points.push(point);
   }
   
   // Debug
   // this.zone.setPath(points);
   
   return points;
}

WateringZone.prototype.debug = function () {
   return "this.side1 = " + this.side1 + "</br>this.distanceOrAngle1 = " + this.distanceOrAngle1 + "</br>this.side2 = " + this.side2 + "</br>this.distanceOrAngle2 = " + this.distanceOrAngle2;
}
